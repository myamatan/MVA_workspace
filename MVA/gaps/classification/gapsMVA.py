#!/usr/bin/env python
import sys

# # Select Theano as backend for Keras
# from os import environ
# environ['KERAS_BACKEND'] = 'theano'
# 
# # Set architecture of system (AVX instruction set is not supported on SWAN)
# environ['THEANO_FLAGS'] = 'gcc.cxxflags=-march=corei7'

from ROOT import TMVA, TFile, TTree, TCut, TString
from ROOT import gRandom, gROOT
from subprocess import call
from os.path import isfile

# from keras.models import Sequential, Model
# from keras.layers.core import Dense
# from keras.optimizers import Adam
# 
# from keras.layers.advanced_activations import LeakyReLU
# from keras.layers.recurrent import LSTM, SimpleRNN
# from keras.layers import Dense, Activation, BatchNormalization, Dropout, Input, Lambda
# from keras.layers import Conv2D, MaxPooling2D, Flatten, Reshape, UpSampling2D, merge
# 
# from keras.optimizers import SGD
# from keras.datasets import mnist

# from modelMVA import ResNetBlock, myDNN 

if __name__ == "__main__":

  # args = sys.argv
  # if len(args) > 1 :
  #   gp = args[1]
  #   regime = args[2]
  #   mass = args[3]
  #   mc = args[4]
  #   out = args[5]
  # else:
  #   gp = 'VBFH'
  #   regime = 'Resolved'
  #   mass = '700'
  #   mc = 'ad'
  #   out = '.myDNN'
  out = '_'
  out += 'umb' # umb, cub, trk
  
  gRandom.SetSeed(0)

  signalID = 'antideteron_b0106_isotropicOn'
  # signalID = gp + '_' + regime + '_SRCR_' + mass
  # signalName = signalID + '/signal.' + mc
  # bkgName = gp + '_' + regime + '_SRCR_Zjets.' + mc

  # Local
  signalName = 'evInfo_anti_deuteron_gaps_FTFP_BERT_HP_1591666046'
  bkgName = 'evInfo_anti_proton_gaps_FTFP_BERT_HP_1591666452'
  signalName = 'evInfo_crane_dbar_UMBreco'
  bkgName = 'evInfo_crane_pbar_UMBreco'

  # # KU 
  # signalName='evInfo_anti_proton_gaps_FTFP_BERT_HP_1592542664'
  # #signalName='evInfo_anti_He3_gaps_FTFP_BERT_HP_1592542664'
  # bkgName='evInfo_anti_deuteron_gaps_FTFP_BERT_HP_1592542667'

  # Load data
  #path = '/Users/masahiroyamatani/jaxa/GAPS/work/20200512/run/myAna/input/'
  path = '/Users/masahiroyamatani/jaxa/GAPS/work/20200512/run/gaps_plottingscripts/output/'
  dataS = TFile.Open(path + signalName + '.root')
  signal = dataS.Get('outTree')
  dataB = TFile.Open(path + bkgName + '.root')
  background = dataB.Get('outTree')

  # Setup TMVA
  TMVA.PyMethodBase.PyInitialize()
  output = TFile.Open('./outputMVA/'+signalID+out+'.root', 'RECREATE')
  factory = TMVA.Factory('TMVAClassification', output,
              '!V:!Silent:Color:DrawProgressBar:Transformations=G:AnalysisType=Classification')

  # Number of training sample
  preSel = 'AnhType>-1 && AnhPosID>-1'
  if out.find('umb')!=-1:
    preSel = 'AnhPosID==0'
  elif out.find('cub')!=-1:
    preSel = 'AnhPosID==1'
  elif out.find('trk')!=-1:
    preSel = 'AnhPosID==2'
  else:
    preSel = ""
  nSF = 1.9

  signal.Draw(">>elistS",preSel)
  background.Draw(">>elistB",preSel)
  elistS = gROOT.FindObject("elistS");
  elistB = gROOT.FindObject("elistB");
  nSig = elistS.GetN()
  nBkg = elistB.GetN()
  if nSig < nBkg:
    nTrainSig = nSig * 0.8
    nTrainBkg = nTrainSig
  else:
    nTrainBkg = nBkg * 0.8
    nTrainSig = nTrainBkg
  nTestSig  = nSig - nTrainSig
  nTestBkg  = nBkg - nTrainBkg

  
  nInput = 5
  dataloader = TMVA.DataLoader('weights')
  #dataloader.AddVariable('nTotalHits')
  #dataloader.AddVariable('nUmbrellaHits')
  #dataloader.AddVariable('nCubeHits')
  #dataloader.AddVariable('nTrackerHits')
  #dataloader.AddVariable('SumOfTotalEnergyDeposition')
  dataloader.AddVariable('nTotalHits_rec')
  dataloader.AddVariable('nUmbrellaHits_rec')
  dataloader.AddVariable('nCubeHits_rec')
  dataloader.AddVariable('nTrackerHits_rec')
  dataloader.AddVariable('totalEnergyDeposition_rec')
  dataloader.AddVariable('umbEnergyDeposition_rec')
  dataloader.AddVariable('cubEnergyDeposition_rec')
  dataloader.AddVariable('trkEnergyDeposition_rec')
  #dataloader.AddVariable('myvar1 := cos(Theta1)','F')

  dataloader.AddSignalTree(signal, 1.0)
  dataloader.AddBackgroundTree(background, 1.0)


  dataloader.PrepareTrainingAndTestTree(TCut(preSel),
          'nTrain_Signal='+str(nTrainSig)+':nTrain_Background='+str(nTrainBkg)+':nTest_Signal='+str(nTestSig)+':nTest_Background='+str(nTestBkg)+':SplitMode=Random:NormMode=NumEvents:!V')

  # ## My DNN
  # # Generate model
  # model = myDNN(nInput)
  # # Store model to file
  # model.save('model.h5')
  # model.summary()
  # factory.BookMethod(dataloader, TMVA.Types.kPyKeras, 'PyKeras',
  #         'H:!V:VarTransform=G:FilenameModel=model.h5:NumEpochs=10:BatchSize=32')

  # Book methods
  print signalID
  factory.BookMethod(dataloader, TMVA.Types.kBDT, 'BDTG_gaps'+out,
          '!H:!V:NTrees=1000:MinNodeSize=2.5%:BoostType=Grad:Shrinkage=0.1:UseBaggedBoost:BaggedSampleFraction=0.5:nCuts=20:MaxDepth=3')

  # Run training, test and evaluation
  factory.TrainAllMethods()
  factory.TestAllMethods()
  factory.EvaluateAllMethods()
