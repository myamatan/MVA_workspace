void SetHistoStyle(TH1D *h, TString fileName, Color_t col, bool isTrain){
  	h->SetStats(0);
       	h->SetTitle(fileName);
        h->SetLineColor(col);
	if( isTrain ){
		//h->SetFillStyle(3003);
		//h->SetFillColor(col);
	}else{
		h->SetMarkerStyle(2);
		h->SetMarkerColor(col);
	}
        h->SetLineWidth(2);
	h->GetXaxis()->SetRangeUser(-1.0,1.0);
        h->GetXaxis()->SetTitle("BDT Discriminant");
}
	

//root -b -q 'plot_BDTScore.cc("ggH","Resolved","700")'

void plot_score(){

	TString annPos = "umb";
	TString fileName = "dbar_reco_01M_"+annPos;

	TString method = "BDTG_gaps_"+annPos;
	//TString method = "PyKeras";

        TFile *f = new TFile("./outputMVA/" +fileName+".root");
        
	std::cout << "fileName : ./outputMVA/" << fileName << std::endl;

	//gDirectory->cd("weights");
	gDirectory->cd("dataset");

	//gDirectory->cd("Method_"+method);
	//TDirectoryFile *df = (TDirectoryFile*)gDirectory->Get(method);

	TTree *trainTree = (TTree*)f->Get("TrainTree");
	TTree *testTree  = (TTree*)f->Get("TestTree");

	/*
        h_bkg = (TH1D*)df->Get("MVA_"+method+"_B");
       	h_Train_sig = (TH1D*)df->Get("MVA_"+method+"_Train_S");
        h_Train_bkg = (TH1D*)df->Get("MVA_"+method+"_Train_B");
        h_roc = (TH1D*)df->Get("MVA_"+method+"_rejBvsS");

        TCanvas *cv = new TCanvas();
        cv->cd();

	TLegend *leg = new TLegend(0.6,0.7,0.88,0.88);
        leg->SetBorderSize(0);
        leg->SetFillStyle(0);

        cv->SetTitle(fileName);

	double sigMax = h_sig->GetMaximum();
	double bkgMax = h_bkg->GetMaximum();

	if( sigMax > bkgMax ){
		SetHistoStyle(h_sig, fileName+roc+sigRate, kBlue, false);
		h_sig->GetYaxis()->SetRangeUser(0., sigMax*1.2);
        	h_sig->Draw("E0");
		SetHistoStyle(h_bkg, fileName+roc+sigRate, kRed, false);
        	h_bkg->Draw("E0 same");
	}else{
		SetHistoStyle(h_bkg, fileName+roc+sigRate, kRed, false);
		h_bkg->GetYaxis()->SetRangeUser(0., bkgMax*1.2);
        	h_bkg->Draw("E0");
		SetHistoStyle(h_sig, fileName+roc+sigRate, kBlue, false);
        	h_sig->Draw("E0 same");
	}
	SetHistoStyle(h_Train_sig, fileName+roc+sigRate, kBlue, true);
        h_Train_sig->Draw("HIST same");
	SetHistoStyle(h_Train_bkg, fileName, kRed, true);
        h_Train_bkg->Draw("HIST same");

	leg->AddEntry(h_Train_sig, "Signal (training)", "lf");
	leg->AddEntry(h_sig, "Signal (test)", "p");
	leg->AddEntry(h_Train_bkg, "Background (training)", "lf");
	leg->AddEntry(h_bkg, "Background (test)", "p");
	leg->Draw();

        cv->Print("./outputMVA/plots/"+ fileName + ".png");
	*/
}
